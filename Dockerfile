FROM mambaorg/micromamba
USER root
WORKDIR /app
RUN apt update \
    && apt install -y curl \
    && rm -rf /var/lib/apt/lists/* \
    && curl -L $(curl https://quarto.org/docs/download/_prerelease.json | grep -oP "(?<=\"download_url\":\s\")https.*${ARCH}\.deb") -o /tmp/quarto.deb \
    && dpkg -i /tmp/quarto.deb \
    && rm /tmp/quarto.deb

# RUN apt-get update && apt-get install -y gcc python3-dev && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY env.yml /app/env.yml
RUN micromamba create -f env.yml
COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock
# COPY README.md ./
# COPY requirements.txt ./
COPY docs /app/docs
RUN micromamba run -n ods poetry install --with dev

# COPY --chown=$MAMBA_USER:$MAMBA_USER env.yaml /tmp/env.yaml
RUN micromamba install --yes --file /app/env.yml && \
    micromamba clean --all --yes
ARG MAMBA_DOCKERFILE_ACTIVATE=1  # (otherwise python will not be found)
# RUN python -c 'import uuid; print(uuid.uuid4())' > /tmp/my_uuid
# COPY ./app.py ./

# ENTRYPOINT [ "jupyter",  "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root"]

