**Ruff linter**

Getting Started
If you haven’t got Ruff already, you’ll need to install it.

    pip install ruff

If you do already have Ruff, be sure to upgrade to latest so you can give the formatted version a try!

Formatting with Ruff
To format your code with Ruff, you need to run ruff format.

Here’s an example of formatting your current folder:

ruff format .
The official [announcement](https://astral.sh/blog/the-ruff-formatter) for the Ruff formatter shows it formatting Django, Zulip and other Python projects in less than a second.

Ruff is Configurable
The Ruff formatter adds some configuration options that are absent from Black. Namely, you can tweak your preferred quote and indentation style:

```
[tool.ruff.format]
quote-style = "single"
indent-style = "tab"
```




**Black formatter**

Configure Black Formatter in Settings File
The settings file is in json format, so you need to enclose everything in a curly bracket ({ }). Let's first set the default formatter to use Black; you can do that by typing this into the settings.json:

```
{
   "editor.defaultFormatter": "ms-python.black-formatter",
}
```


If you want VSCode to format on save, you need to write this:

```
{
   "editor.defaultFormatter": "ms-python.black-formatter",
   "editor.formatOnSave": true, // new
}
```


You can enforce a line length by passing an argument to Black Formatter:

```
{
   // ...
   "black-formatter.args": [
    "--line-length", "88",
   ],
}
```


(The line length is 88 by default, which seems to be the recommended length.)

If you want the settings to apply to specific language such as "Python", you can do that like this:

```
{
   // ...
   "[python]": {
    "editor.codeActionsOnSave": {
      "source.organizeImports": true
    },
   },
}
```


The above code organize import statements when you save Python files. You can add previous settings here instead so it only applies to Python file (don't forget the commas):

```
{
   // ...
   "[python]": {
    "editor.codeActionsOnSave": {
      "source.organizeImports": true
    },
    "editor.formatOnSave": true, // new
   },
}
```

